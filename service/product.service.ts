import { BASE_URL } from "./auth.service";

export type product = {
  id: number;
  title: string;
  description: string;
  price: number;
  discountPercentage: number;
  rating: number;
  stock: number;
  brand: string;
  category: string;
  thumbnail: string;
  images: string[];
};

const products = {
  getAll: async (params?: any) => {
    try {
      const param = convertToQueryParams(params);
      const data = await fetch(BASE_URL + "products" + param);
      const json = await data.json();
      return json.products as product[];
    } catch (error) {
      console.log(error);
    }
  },
  getDetailProduct: async (id: any) => {
    try {
      const data = await fetch(BASE_URL + "products/" + id);
      const json = await data.json();
      return json;
    } catch (error) {
      console.log(error);
    }
  },
  getProductCategories: async () => {
    try {
      const data = await fetch(BASE_URL + "products/categories");
      const json = await data.json();
      return json as string[];
    } catch (error) {
      console.log(error);
    }
  },
};

export default products;

const convertToQueryParams = (object: any) => {
  return "?" + new URLSearchParams(object).toString();
};
