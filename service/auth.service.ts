export const BASE_URL = "https://dummyjson.com/";

const auth = {
  login: async (body: any) => {
    try {
      const data = await fetch(BASE_URL + "auth/login", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body),
      });
      const json = await data.json();
      return json;
    } catch (error) {
      console.log(error);
    }
  },
};

export default auth;
