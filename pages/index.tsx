import Image from "next/image";
import { Poppins } from "next/font/google";
import { useState } from "react";
import auth from "@/service/auth.service";
import { useDisclosure } from "@mantine/hooks";
import { Modal, Group, Button } from "@mantine/core";
import { useRouter } from "next/router";
import { useMediaQuery } from "react-responsive";

const poppins = Poppins({ subsets: ["latin"], weight: ["400", "500"] });

export default function Home() {
  const router = useRouter();
  const isBigScreen = useMediaQuery({ query: "(min-width: 770px)" });
  const [formValue, setFormValue] = useState({ username: "", password: "" });
  const [opened, { open, close }] = useDisclosure(false);
  const [openedlogin, { open: openlogin, close: closelogin }] =
    useDisclosure(false);

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    if (formValue.username == "" || formValue.password == "") {
      open();
      return;
    }

    const submit = await auth.login(formValue);

    if (submit.message == "Invalid credentials") {
      openlogin();
      return;
    }
    router.push("/dashboard");
  };
  return (
    <main
      className={`flex min-h-screen flex-col items-center justify-between w-full bg-white text-black ${poppins.className}`}
    >
      <div className="w-full flex items-center flex-col">
        <div className="absolute left-0 w-32 h-32">
          <Image src="/images/header-login.png" alt="" fill />
        </div>
        <div className="relative w-32 h-32">
          <Image
            src="/images/logo.png"
            alt=""
            fill
            style={{ objectFit: "contain" }}
          />
        </div>
        <form
          onSubmit={handleSubmit}
          className={`space-y-4 ${isBigScreen ? "w-[40%]" : "w-[70%]"} `}
        >
          <div className="space-y-2">
            <div className="font-bold text-xl">Login</div>
            <div className="text-xs">Please sign in to continue</div>
          </div>
          <div className="flex flex-col space-y-1">
            <label htmlFor="username" className="font-light">
              User ID
            </label>
            <input
              type="text"
              id="username"
              onChange={(e: any) =>
                setFormValue((prev) => ({ ...prev, username: e.target.value }))
              }
              className="placeholder:text-xs text-xs border-b-gray-300 outline-none border-solid border border-x-0 border-t-0"
              placeholder="User ID"
            />
          </div>
          <div className="flex flex-col space-y-1">
            <label htmlFor="password" className="font-light">
              Password
            </label>
            <input
              type="text"
              id="password"
              onChange={(e: any) =>
                setFormValue((prev) => ({ ...prev, password: e.target.value }))
              }
              className="placeholder:text-xs text-xs border-b-gray-300 outline-none border-solid border border-x-0 border-t-0"
              placeholder="Password"
            />
          </div>
          <div className="w-full flex justify-end">
            <button className="bg-[#6338A1] text-white px-10 py-2 font-medium tracking-wider rounded-full">
              LOGIN
            </button>
          </div>
        </form>
      </div>
      <div className="text-center mb-4 text-gray-500">
        Don&rsquo;t have an account?
        <span className="text-orange-500 font-semibold"> Sign up</span>
      </div>
      <Modal opened={opened} onClose={close} centered withCloseButton={false}>
        <div className="flex flex-col space-y-4">
          <div>User ID dan atau Password anda belum diisi</div>
          <Button
            variant="outline"
            color="grape"
            onClick={close}
            className="w-fit"
          >
            Tutup
          </Button>
        </div>
      </Modal>
      <Modal
        opened={openedlogin}
        onClose={closelogin}
        centered
        withCloseButton={false}
      >
        <div className="flex flex-col space-y-4">
          <div>User ID atau Password anda salah</div>
          <Button
            variant="outline"
            color="grape"
            onClick={closelogin}
            className="w-fit"
          >
            Tutup
          </Button>
        </div>
      </Modal>
    </main>
  );
}
