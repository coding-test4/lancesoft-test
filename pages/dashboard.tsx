import products, { product } from "@/service/product.service";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

const Dashboard = (props: any) => {
  const router = useRouter();
  const [product, setProduct] = useState<product[]>();
  const getProducts = async () => {
    const data = await products.getAll({ limit: 100 });
    setProduct(data);
  };

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <div className="bg-white text-black p-4 space-y-4">
      <div className="font-bold tracking-wider">DASHBOARD</div>
      <div className="flex overflow-auto space-y-3 space-x-3 flex-wrap justify-center">
        {product?.map((item, key) => (
          <div
            key={key}
            className="w-60 max-h-52 h-fit flex cursor-pointer hover:shadow-lg bg-white p-4 shadow-md shadow-gray-200 rounded-lg flex-col items-center text-ellipsis overflow-hidden whitespace-nowrap"
          >
            <div className="relative w-20 h-20">
              <Image
                alt=""
                src={item.thumbnail}
                fill
                style={{ objectFit: "cover" }}
              />
            </div>
            <div className="flex items-center justify-between w-full">
              <div className="w-[80%] font-semibold text-ellipsis overflow-hidden whitespace-nowrap">
                {item.title}
              </div>
              <div className="w-[20%] text-center font-semibold">
                ${item.price}
              </div>
            </div>
            <div className="text-ellipsis w-[80%] h-fit overflow-hidden whitespace-nowrap">
              {item.description}
            </div>
            <button
              className="px-6 tracking-wider font-semibold py-2 mt-4 bg-gray-400 rounded-full"
              onClick={() => router.push("/product/" + item.id)}
            >
              DETAIL
            </button>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Dashboard;
