import products, { product } from "@/service/product.service";
import { Carousel } from "@mantine/carousel";
import { Popover, Text } from "@mantine/core";
import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useMediaQuery } from "react-responsive";

const DetailProduct = () => {
  const router = useRouter();
  const { id } = router.query;
  const [product, setProduct] = useState<product>();
  const [info, setInfo] = useState({ qty: 1, price: 0 });
  const isBigScreen = useMediaQuery({ query: "(min-width: 770px)" });

  const getProduct = async (id: string) => {
    const data = await products.getDetailProduct(id);
    setInfo((prev) => ({ ...prev, price: data.price }));
    setProduct(data);
  };

  useEffect(() => {
    if (id) {
      getProduct(id.toString());
    }
  }, [id]);
  return (
    product &&
    (isBigScreen ? (
      <div className="w-full h-screen flex items-center bg-white text-black">
        <div className="flex flex-col items-center w-[40%] space-y-4">
          <Carousel
            maw={300}
            mx="auto"
            withIndicators
            height={300}
            loop
            slideGap={"lg"}
          >
            {product.images.map((image, key) => (
              <Carousel.Slide
                key={key}
                className="flex justify-center items-center shadow-md shadow-gray-300"
              >
                <div className="relative w-80 h-80">
                  <Image
                    alt=""
                    src={image}
                    fill
                    style={{ objectFit: "contain" }}
                  />
                </div>
              </Carousel.Slide>
            ))}
          </Carousel>
          <div className="min-w-[300px] rounded-lg shadow-md shadow-gray-300 px-4 py-5 space-y-4">
            <div className="flex w-full justify-between">
              <div className="flex items-center space-x-2">
                <div
                  onClick={() =>
                    info.qty > 0 &&
                    setInfo((prev) => ({
                      price: (prev.qty - 1) * product.price,
                      qty: prev.qty - 1,
                    }))
                  }
                  className="w-6 h-6 grid place-content-center rounded-lg bg-orange-200 cursor-pointer"
                >
                  -
                </div>
                <div>{info.qty}</div>
                <div
                  onClick={() =>
                    setInfo((prev) => ({
                      price: (prev.qty + 1) * product.price,
                      qty: prev.qty + 1,
                    }))
                  }
                  className="w-6 h-6 grid place-content-center rounded-lg bg-orange-200 cursor-pointer"
                >
                  +
                </div>
              </div>
              <div>${info.price}</div>
            </div>
            <Popover>
              <Popover.Target>
                <button
                  className="w-full bg-orange-500 rounded-lg text-white py-2 font-semibold disabled:bg-orange-200 transition-all duration-200"
                  disabled={info.price == 0}
                >
                  BUY
                </button>
              </Popover.Target>
              <Popover.Dropdown>
                <Text>Under development⚡</Text>
              </Popover.Dropdown>
            </Popover>
          </div>
        </div>
        <div className="w-[60%] flex items-center flex-col space-y-4">
          <div className="w-full flex items-center text-3xl">
            <div className="w-[50%]">
              <div className="font-semibold w-full">{product.title}</div>
              <div className="font-semibold w-full">⭐{product.rating}</div>
            </div>
            <div className="w-[50%] text-center font-semibold">
              ${product.price}
            </div>
          </div>
          <div className="w-full">{product.description}</div>
        </div>
      </div>
    ) : (
      <div className="w-full h-screen space-y-4 pt-4 flex items-center flex-col bg-white text-black">
        <div className="flex w-full">
          <Carousel
            maw={300}
            mx="auto"
            withIndicators
            height={300}
            loop
            slideGap={"lg"}
          >
            {product.images.map((image, key) => (
              <Carousel.Slide
                key={key}
                className="flex justify-center items-center shadow-md shadow-gray-300"
              >
                <div className="relative w-80 h-80">
                  <Image
                    alt=""
                    src={image}
                    fill
                    style={{ objectFit: "contain" }}
                  />
                </div>
              </Carousel.Slide>
            ))}
          </Carousel>
        </div>
        <div className="w-full h-fit px-4 flex items-center flex-col space-y-4">
          <div className="w-full flex items-center text-3xl">
            <div className="w-[80%]">
              <div className="font-semibold w-full">{product.title}</div>
              <div className="font-semibold w-full">⭐{product.rating}</div>
            </div>
            <div className="w-[20%] text-center font-semibold">
              ${product.price}
            </div>
          </div>
          <div className="w-full">{product.description}</div>
        </div>
        <div className="w-[90%]  rounded-lg shadow-md shadow-gray-300 px-4 py-5 space-y-4">
          <div className="flex w-full justify-between">
            <div className="flex items-center space-x-2">
              <div
                onClick={() =>
                  info.qty > 0 &&
                  setInfo((prev) => ({
                    price: (prev.qty - 1) * product.price,
                    qty: prev.qty - 1,
                  }))
                }
                className="w-6 h-6 grid place-content-center rounded-lg bg-orange-200 cursor-pointer"
              >
                -
              </div>
              <div>{info.qty}</div>
              <div
                onClick={() =>
                  setInfo((prev) => ({
                    price: (prev.qty + 1) * product.price,
                    qty: prev.qty + 1,
                  }))
                }
                className="w-6 h-6 grid place-content-center rounded-lg bg-orange-200 cursor-pointer"
              >
                +
              </div>
            </div>
            <div>${info.price}</div>
          </div>
          <Popover>
            <Popover.Target>
              <button
                className="w-full bg-orange-500 rounded-lg text-white py-2 font-semibold disabled:bg-orange-200 transition-all duration-200"
                disabled={info.price == 0}
              >
                BUY
              </button>
            </Popover.Target>
            <Popover.Dropdown>
              <Text>Under development⚡</Text>
            </Popover.Dropdown>
          </Popover>
        </div>
      </div>
    ))
  );
};

export default DetailProduct;
